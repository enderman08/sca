#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include <assert.h>
#include <time.h>


#define CHUNK_SIZE 10

// map[height][widht]

typedef struct map_cell
{
	unsigned char entity;
	unsigned char symbol;
} map_cell_t;

typedef struct map
{
	unsigned short w;
	unsigned short h;
	map_cell_t **cells;
} map_t;

map_cell_t **map_cells_init(unsigned short w, unsigned short h)
{
	map_cell_t **c = NULL;

	c = calloc(h, sizeof(map_cell_t*));

	for(int i = 0; i < h; i++)
	{
		c[i] = calloc(w, sizeof(map_cell_t));
	}

	return c;
}

int map_init(map_t *m, unsigned short w, unsigned short h)
{
	m->w = w;
	m->h = h;
	m->cells = map_cells_init(m->w, m->h);

	return EXIT_SUCCESS;
}

bool isprintable(char c)
{
	switch(c)
	{
		case '.':
		case '#':
		case '@':
			return true;
	}
	return false;
}

map_cell_t *map_cells_get(map_t *m, unsigned short w, unsigned short h)
{
	return &m->cells[h + CHUNK_SIZE][w + CHUNK_SIZE];
}

unsigned char celltochar(map_t *m, unsigned short w, unsigned short h)
{
	unsigned char c = 0;
	unsigned char t = 0;

	t = map_cells_get(m, w, h)->symbol;

	if(isprintable(t)) c = t;

	t = map_cells_get(m, w, h)->entity;

	if(isprintable(t)) c = t;


	return c;
}

void map_cells_fill(map_t *map, char c)
{
	for(unsigned int i = 0; i < map->h; i++)
	{
		for(unsigned int j = 0; j < map->w; j++)
		{
			map->cells[i][j].symbol = c;
		}
	}
}

void map_cells_rand(map_t *map, char c, char r)
{
	for(unsigned int i = 0; i < map->h; i++)
	{
		for(unsigned int j = 0; j < map->w; j++)
		{
			map->cells[i][j].symbol = rand() % r == 0? c : map->cells[i][j].symbol;
		}
	}
}

void map_print(map_t *map)
{
	unsigned short h_st = 0;
	unsigned short h_en = map->h - 2 * CHUNK_SIZE;

	unsigned short w_st = 0;
	unsigned short w_en = map->w - 2 * CHUNK_SIZE;

	for(unsigned int i = h_st; i < h_en; i++)
	{
		for(unsigned int j = w_st; j < w_en; j++)
		{
// 			printf("%2c", map->cells[i][j].symbol);
// 			printw("%c", map->cells[i][j].symbol);
// 			mvaddch(i, j, map->cells[i][j].symbol);
// 			addch(map->cells[i][j].symbol);

			mvaddch(i, j, celltochar(map, j, i));

		}
// 		puts(" !");
	}
}

void map_destroy(map_t *m)
{
	for(int i = 0; i < m->h; i++)
	{
		free(m->cells[i]);
	}
	free(m->cells);
}

int main()
{
	unsigned short w = 0;
	unsigned short h = 0;

	initscr();			/* Start curses mode 		  */
	raw();
	noecho();
	keypad(stdscr, TRUE);

	getmaxyx(stdscr, h, w);

	assert(w != 0 && h != 0);

	w += 2 * CHUNK_SIZE;
	h += 2 * CHUNK_SIZE;

	srand(time(NULL));

	map_t map = {0};

	map_init(&map, w, h);

	map_cells_fill(&map, '.');

	map_cells_rand(&map, '#', 10);

	map_print(&map);

	map_destroy(&map);
	refresh();			/* Print it on to the real screen */
	getch();			/* Wait for user input */
	endwin();			/* End curses mode		  */

	return EXIT_SUCCESS;
}
